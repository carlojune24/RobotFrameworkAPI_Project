*** Variables ***
${LOGIN_EMAIL}          css=#login-email
${LOGIN_PASSWORD}       css=#login-password
${LOGIN_BUTTON}         css=#signin-button
${LOGIN_RESULT}         css=#login-result
${BROWSER_CHROME}       chrome
${HOME_PAGE}            https://www.takatack.com
${LOGIN_PAGE}           css=#login-link
${SELENIUM_TIMEOUT}     10
${SELENIUM_SPEED}       .2