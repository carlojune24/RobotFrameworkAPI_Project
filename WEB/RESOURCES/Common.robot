*** Settings ***
Library     Selenium2Library

Resource    ${EXECDIR}${/}RESOURCES${/}ResourceMapping.robot
Variables   ${EXECDIR}${/}INPUTS${/}Logincredintials.py
Resource    ${EXECDIR}${/}RESOURCES${/}PageObjects${/}HomePage.robot
Resource    ${EXECDIR}${/}TESTS${/}LoginTemplate${/}LoginValidation.robot
Resource    ${EXECDIR}${/}CONFIG${/}Setup.robot

*** Keywords ***

Begin Test
    Setup.Start With Chrome
    Go To                       ${HOME_PAGE}
End Test
    Setup.End
