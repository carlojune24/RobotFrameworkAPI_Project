*** Settings ***
Resource        ${EXECDIR}${/}RESOURCES${/}Common.robot
Test Setup      Common.Begin Test
Test Teardown   Common.End Test

*** Test Case ***
Login Validation
    HomePage.Go To Home Page
    Homepage.Click Login/Register
    LoginValidation.Login Invalid
    LoginValidation.Login Valid

