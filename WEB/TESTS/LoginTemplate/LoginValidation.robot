*** Settings ***
Resource    ${EXECDIR}${/}RESOURCES${/}Common.robot

*** Keywords ***
Login Valid
    Input Text      ${LOGIN_EMAIL}      ${LOGIN_VALID["email"]}
    Input Password  ${LOGIN_PASSWORD}   ${LOGIN_VALID["password"]}
    Click Element   ${LOGIN_BUTTON}
#   Wait Until Element Contains     ${LOGIN_RESULT}
Login Invalid
    Input Text      ${LOGIN_EMAIL}      ${LOGIN_INVALID["email"]}
    Input Password  ${LOGIN_PASSWORD}   ${LOGIN_INVALID["email"]}
    Click Element   ${LOGIN_BUTTON}
    Wait Until Element Contains     ${LOGIN_RESULT}     ${LOGIN_INVALID["login_result"]}