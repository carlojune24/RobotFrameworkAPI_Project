*** Settings ***
Library     Collections
Library     RequestsLibrary

*** Testcases ***
Post Requests
    [Tags]  post
    Create Session      tg      https://post.chikka.com/smsapi
    ${params}=           Create Dictionary       message_type=SEND       mobile_number=639501461154      shortcode=29290242      message_id=carlo123     message=carlo       client_id=4be33cbeb17daac5c9d7a8f46238a027505db9c7b1fc748e8c2202ca99ddee2c     secret_key=cee3d1a47896fbc9cdb7d555997de99fb87a8c03db2a55329638ffc15d182947
    ${headers}=        Create Dictionary       Content-type=application/x-www-form-urlencoded
    ${resp}=           Post Request            tg     /request         data=${params}        headers=${headers}
    Should Be Equal As Strings                  ${resp.status_code}     200
    Log To Console      ${resp}